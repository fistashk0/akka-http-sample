name := "akka-http-sample"

lazy val library = new {

  object Version {
    val akka = "2.6.0"
    val akkaHttp = "10.1.10"
    val logbackVersion = "1.2.3"
    val slick = "3.2.3"
    val slickPgVersion = "0.17.2"
  }

  lazy val akka: Seq[ModuleID] = Seq(
    "com.typesafe.akka" %% "akka-actor" % Version.akka,
    "com.typesafe.akka" %% "akka-stream" % Version.akka,
    "com.typesafe.akka" %% "akka-slf4j" % Version.akka,
    "com.typesafe.akka" %% "akka-testkit" % Version.akka % Test
  )

  val akkaHttp: Seq[ModuleID] = Seq(
    "com.typesafe.akka" %% "akka-http" % Version.akkaHttp,
    "com.typesafe.akka" %% "akka-http-core" % Version.akkaHttp,
    "com.typesafe.akka" %% "akka-http-spray-json" % Version.akkaHttp,
    "com.typesafe.akka" %% "akka-http-testkit" % Version.akkaHttp % "test"
  )

  val slick: Seq[ModuleID] = Seq(
    "com.typesafe.slick" %% "slick" % Version.slick,
    "com.typesafe.slick" %% "slick-hikaricp" % Version.slick,
    "org.postgresql" % "postgresql" % "9.4.1211",
    "com.h2database" % "h2" % "1.4.192" % "test",
    "com.beachape" %% "enumeratum-slick" % "1.5.16",
    "com.beachape" %% "enumeratum-circe" % "1.5.21",
    "com.github.tminglei" %% "slick-pg" % Version.slickPgVersion,
    "com.github.tminglei" %% "slick-pg_circe-json" % Version.slickPgVersion
  )
}

libraryDependencies ++= library.akka ++ library.akkaHttp ++ library.slick