package ru.company.api

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import ru.company.api.Models.PetKindEnum.Cat
import ru.company.api.config.ConfigModule
import ru.company.api.db.SlickDatabaseModuleDefault
import ru.company.api.modules.APIRequestReceiver
import slick.jdbc.PostgresProfile.backend._

import scala.concurrent.ExecutionContext
import scala.language.postfixOps


object Main
  extends App
    with ConfigModule
    with APIRequestReceiver {

  implicit val actorSystem: ActorSystem = ActorSystem("rabbit-system")
  implicit val executionContext: ExecutionContext = actorSystem.dispatcher
  implicit val materializer: Materializer = ActorMaterializer()

  implicit val dbModule: DatabaseDef = SlickDatabaseModuleDefault.db
  val api = this.launchAPI()

  println("Application started")

  scala.sys.addShutdownHook {
    api
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => actorSystem.terminate()) // and shutdown when done

  }
}