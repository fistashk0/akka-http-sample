package ru.company.api.modules

import java.sql.Date

import ru.company.api.Models.PetKindEnum

object Models {
  case class UserInfo(
    lastName: String,
    firstName: String,
    patronymic: String,
    mailAddress: String,
    country: String,
    city: String,
    street: String,
    building: String,
    apartment: String,
    born: Date,
    passportNumber: String,
    address: String,
    departmentCode: String,
    issuedBy: String
  )

  case class PetInfo(
    owner: Long,
    nickname: String,
    born: Date,
    breed: String,
    vaccinations: Option[String],
    kind: PetKindEnum
  )

}


