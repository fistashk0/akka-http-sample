package ru.company.api.modules

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{entity, _}
import akka.http.scaladsl.server.{ExceptionHandler, PathMatchers}
import akka.stream.Materializer
import ru.company.api.Models.PetKindEnum.{Cat, Dog}
import ru.company.api.Models.PetModel
import ru.company.api.config.ConfigModule
import ru.company.api.db.{PetRepository, UserRepository}
import ru.company.api.json.JsonSupport
import ru.company.api.modules.Models.{PetInfo, UserInfo}
import slick.jdbc.PostgresProfile.backend._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal


trait APIRequestReceiver extends ConfigModule with JsonSupport {

  implicit def executionContext: ExecutionContext
  implicit def actorSystem: ActorSystem
  implicit def materializer: Materializer
  implicit val dbModule: DatabaseDef

  private val port = config.getInt("API.port")

  private[modules] lazy val route = handleExceptions(customExceptionHandler) {
    concat(
      path("health") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>OK!</h1>"))
        }
      },
      path("user") {
        concat(
          get {
            concat(
              parameters('email.as[String]) { email =>
                complete(UserRepository.findUserByEmail(email))
              },
              parameters('passportNumber.as[String]) { passportNumber =>
                complete(UserRepository.findUserByPassportNumber(passportNumber))
              }
            )
          },
          put {
            entity(as[UserInfo]) { user =>
              UserRepository.updateOrCreate(user)
              complete(s"successful")
            }
          }
        )
      },
      path("user" / "pet") {
        concat(
          get {
            parameters('ownerId.as[Long], 'nickname.as[String]) { case (ownerId, nickname) =>
              complete(PetRepository.get(ownerId, nickname))
            }
          },
          put {
            entity(as[PetInfo]) { pet =>
              onSuccess(PetRepository.insertOrUpdate(PetModel(0L, pet.owner, pet.nickname, pet.born, pet.breed, pet.vaccinations, pet.kind))) { _ =>
                complete(s"successful")
              }
            }
          }
        )
      },
      path("report" / "byPetType" / PathMatchers.Remaining) { petType =>
        get {
          petType match {
            case "cat" => complete(PetRepository.report(Cat))
            case "dog" => complete(PetRepository.report(Dog))
            case _ => complete(StatusCodes.NotFound -> s"Can't found pet with type $petType")
          }
        }
      }
    )
  }

  private def customExceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case NonFatal(ex) =>
        extractUri { _ =>
          complete(StatusCodes.InternalServerError -> s"Internal service error. $ex")
        }
    }

  def launchAPI(): Future[Http.ServerBinding] = {
    println(s"Api started on port : $port")
    Http().bindAndHandle(route, "0.0.0.0", port)
  }
}
