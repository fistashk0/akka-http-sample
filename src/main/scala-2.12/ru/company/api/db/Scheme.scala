package ru.company.api.db

import java.sql.Date

import ru.company.api.Models._
import slick.jdbc.PostgresProfile.api._
import slick.lifted.{ProvenShape, TableQuery}


trait Scheme extends SlickPostgresModule {

  protected val addressTable = TableQuery[AddressTable]
  protected val passportTable = TableQuery[PassportTable]
  protected val userTable = TableQuery[UserTable]
  protected val petTable = TableQuery[PetTable]

  class AddressTable(tag: Tag)
    extends Table[AddressModel](tag, "tbl_address") {
    def id: Rep[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def country: Rep[String] = column[String]("country")
    def city: Rep[String] = column[String]("city")
    def street: Rep[String] = column[String]("street")
    def building: Rep[String] = column[String]("building")
    def apartment: Rep[String] = column[String]("apartment")

    def * : ProvenShape[AddressModel] = (id, country, city, street, building, apartment) <> (AddressModel.tupled, AddressModel.unapply)
  }

  class PassportTable(tag: Tag)
    extends Table[PassportModel](tag, "tbl_passport") {
    def id: Rep[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def lastName: Rep[String] = column[String]("last_name")
    def firstName: Rep[String] = column[String]("first_name")
    def patronymic: Rep[String] = column[String]("patronymic")
    def born: Rep[Date] = column[Date]("born")
    def number: Rep[String] = column[String]("number")
    def address: Rep[String] = column[String]("address")
    def departmentCode: Rep[String] = column[String]("department_code")
    def issuedBy: Rep[String] = column[String]("issued_by")

    def * : ProvenShape[PassportModel] = (id, lastName, firstName, patronymic, born, number, address, departmentCode, issuedBy) <> (PassportModel.tupled, PassportModel.unapply)
  }

  class UserTable(tag: Tag)
    extends Table[UserModel](tag, "tbl_user") {

    def id: Rep[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def lastName: Rep[String] = column[String]("last_name")
    def firstName: Rep[String] = column[String]("first_name")
    def patronymic: Rep[String] = column[String]("patronymic")
    def mailAddress: Rep[String] = column[String]("mail_address")
    def address: Rep[Long] = column[Long]("address")
    def passport: Rep[Long] = column[Long]("passport")

    def * : ProvenShape[UserModel] = (id, lastName, firstName, patronymic, mailAddress, address, passport) <> (UserModel.tupled, UserModel.unapply)

  }

  class PetTable(tag: Tag)
    extends Table[PetModel](tag, "tbl_pet") {
    import enumImplicits._

    def id: Rep[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def owner: Rep[Long] = column[Long]("owner")
    def nickname: Rep[String] = column[String]("nickname")
    def born: Rep[Date] = column[Date]("born")
    def breed: Rep[String] = column[String]("breed")
    def vaccinations: Rep[Option[String]] = column[Option[String]]("vaccinations")
    def kind: Rep[PetKindEnum] = column[PetKindEnum]("kind")(jdbcTypeForEnum("enum__pet_kind_type"))

    def * : ProvenShape[PetModel] = (id, owner, nickname, born, breed, vaccinations, kind) <> (PetModel.tupled, PetModel.unapply)

  }

}
