package ru.company.api

import java.sql.Date

import enumeratum.EnumEntry.Uncapitalised
import enumeratum.{CirceEnum, Enum, EnumEntry}

import scala.collection.immutable

package object Models {

  sealed trait PetKindEnum extends EnumEntry with Uncapitalised

  object PetKindEnum extends Enum[PetKindEnum] with CirceEnum[PetKindEnum] {
    case object Cat extends PetKindEnum
    case object Dog extends PetKindEnum

    override val values: immutable.IndexedSeq[PetKindEnum] = findValues
  }

  case class AddressModel(
    id: Long,
    country: String,
    city: String,
    street: String,
    building: String,
    apartment: String
  )

  case class PassportModel(
    id: Long,
    lastName: String,
    firstName: String,
    patronymic: String,
    born: Date,
    number: String,
    address: String,
    departmentCode: String,
    issuedBy: String
  )

  case class UserModel(
    id: Long,
    lastName: String,
    firstName: String,
    patronymic: String,
    mailAddress: String,
    address: Long,
    passport: Long
  )

  case class PetModel(
    id: Long,
    owner: Long,
    nickname: String,
    born: Date,
    breed: String,
    vaccinations: Option[String],
    kind: PetKindEnum
  )

  case class PetReport(
    petInfo: PetModel,
    email: String
  )

}
