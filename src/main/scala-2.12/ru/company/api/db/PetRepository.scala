package ru.company.api.db

import ru.company.api.Models.{PetKindEnum, PetModel, PetReport}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

object PetRepository extends Scheme {

  def get(owner: Long, nickname: String)(implicit db: Database, ec: ExecutionContext) = {
    db.run(petTable.filter(pet => pet.owner === owner && pet.nickname === nickname).result.headOption)
  }

  def insertOrUpdate(petModel: PetModel)(implicit db: Database, ec: ExecutionContext): Future[Int] = {
    get(petModel.owner, petModel.nickname).flatMap { petOption =>
      db.run(petTable.insertOrUpdate(petOption.map { petInDb => petModel.copy(id = petInDb.id) }.getOrElse(petModel)))
    }
  }

  def report(petKindEnum: PetKindEnum)(implicit db: Database, ec: ExecutionContext): Future[Seq[PetReport]] = {
    import enumImplicits._

    db.run(petTable
           .filter(_.kind === petKindEnum)
           .join(userTable)
           .on(_.owner === _.id)
           .map(x => x._1 -> x._2.mailAddress)
           .result
    ).map(_.map(PetReport.tupled))
  }
}