package ru.company.api.db

import ru.company.api.Models.{AddressModel, PassportModel, UserModel}
import ru.company.api.modules.Models.UserInfo
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

object UserRepository extends Scheme {

  def updateOrCreate(userInfo: UserInfo)(implicit db: Database, ec: ExecutionContext): Future[Long] = {
    findUserByEmail(userInfo.mailAddress).flatMap(user =>
      if (user.isEmpty) findUserByPassportNumber(userInfo.passportNumber)
      else Future(user)
    ).flatMap { userOpt =>
      val query = {
        userOpt.map { user =>
          updateUserModel(userInfo, user.id, user.passport, user.address)
        }.getOrElse {
          val (address, passport) = convert(userInfo)
          for {
            addressId <- addressTable returning addressTable.map(_.id) += address
            passportId <- passportTable returning passportTable.map(_.id) += passport
            result <- {
              val user = UserModel(0L, userInfo.lastName, userInfo.firstName, userInfo.patronymic, userInfo.mailAddress, addressId, passportId)
              userTable returning userTable.map(_.id) += user
            }
          } yield result
        }
      }.transactionally
      db.run(query)
    }
  }

  def updateUserModel(userInfo: UserInfo, userId: Long, passportId: Long, addressId: Long)(implicit db: Database, ec: ExecutionContext) = {
    val (address, passport) = convert(userInfo, addressId, passportId)
    for {
      addressId <- addressTable.update(address)
      passportId <- passportTable.update(passport)
      result <- {
        val user = UserModel(userId, userInfo.lastName, userInfo.firstName, userInfo.patronymic, userInfo.mailAddress, addressId, passportId)
        userTable returning userTable.map(_.id) += user
      }
    } yield result
  }

  def findUserByEmail(email: String)(implicit db: Database, ec: ExecutionContext): Future[Option[UserModel]] = {
    db.run(userTable.filter(_.mailAddress === email).result.headOption)
  }

  def findUserByPassportNumber(passportNumber: String)(implicit db: Database, ec: ExecutionContext): Future[Option[UserModel]] = {
    db.run(
      passportTable.filter(_.number === passportNumber)
      .joinLeft(userTable)
      .on(_.id === _.passport)
      .result
      .headOption
    ).map(_.flatMap(_._2))
  }

  def convert(userInfo: UserInfo, addressId: Long = 0L, passportId: Long = 0L) = {
    AddressModel(
      addressId,
      userInfo.country,
      userInfo.city,
      userInfo.street,
      userInfo.building,
      userInfo.apartment
    ) -> PassportModel(
      passportId,
      userInfo.lastName,
      userInfo.firstName,
      userInfo.patronymic,
      userInfo.born,
      userInfo.passportNumber,
      userInfo.address,
      userInfo.departmentCode,
      userInfo.issuedBy
    )
  }

}