package ru.company.api.db


import com.github.tminglei.slickpg.{PgCirceJsonSupport, PgEnumSupport, _}
import com.typesafe.config.{Config, ConfigFactory}
import enumeratum.{Enum, EnumEntry, SlickEnumSupport}
import ru.company.api.Models.PetKindEnum
import slick.ast.BaseTypedType
import slick.dbio.DBIO
import slick.jdbc
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.{JdbcType, PostgresProfile}

import scala.concurrent.ExecutionContext
import scala.language.implicitConversions
import scala.reflect.ClassTag
import scala.util.Try

trait SlickPostgresModule
  extends PgCirceJsonSupport
    with PgEnumSupport
    with PgDate2Support
    with PgArraySupport
    with PostgresProfile
    with SlickEnumSupport {
  self =>
  val circeJsonImplicits: CirceImplicits = new self.CirceImplicits {}
  val dateTimeImplicits: DateTimeImplicits = new self.DateTimeImplicits {}
  val arrayImplicits: ArrayImplicits = new self.ArrayImplicits {}

  override def pgjson: String = "jsonb"

  def jdbcTypeForEnum[T <: EnumEntry](name: String)(implicit ev: Enum[T], c: ClassTag[T]): JdbcType[T] =
    createEnumJdbcType(
      name,
      _.entryName,
      ev.withName,
      quoteName = false
    )

  object enumImplicits {
    implicit def testStatCT: BaseTypedType[PetKindEnum] =
      MappedColumnType.base[PetKindEnum, String](
        enum => enum.toString, str => PetKindEnum.withName(str)
      )
    implicit lazy val regulationTypeMapper = mappedColumnTypeForEnum(PetKindEnum)
  }

  def tryToEither[A](t: Try[A]): Either[Throwable, A] = t.fold(e => Left(e), r => Right(r))

  implicit final def DBIOTryToEither[A](
    action: DBIO[Try[A]]
  )(implicit ec: ExecutionContext): DBIO[Either[Throwable, A]] =
    action.map(tryToEither)

}

trait SlickDatabaseModule extends PostgresProfile {
  val db: jdbc.PostgresProfile.backend.Database
}

object SlickDatabaseModuleDefault extends SlickDatabaseModule {
  private lazy val configTrackingDb: Config = ConfigFactory.load().getConfig("db")
  private lazy val host: String = configTrackingDb.getString("host")
  private lazy val port: String = configTrackingDb.getString("port") //TODO should NOT be lazy
  private lazy val dbName: String = configTrackingDb.getString("dbname") //TODO remove lazy after sbt 1.*
  private lazy val username: String = configTrackingDb.getString("username")
  private lazy val password: String = configTrackingDb.getString("password")
  private lazy val keepAliveConnection: Boolean = configTrackingDb.getBoolean("keepAliveConnection")
  private lazy val threadPoolSize: Int = configTrackingDb.getInt("threadPoolSize")
  private lazy val driver = "org.postgresql.Driver"
  private lazy val url = s"jdbc:postgresql://$host:$port/$dbName"

  override lazy val db = Database.forURL(
    url,
    user = username,
    password = password,
    keepAliveConnection = keepAliveConnection,
    driver = driver,
    executor = AsyncExecutor("custom", numThreads = threadPoolSize, queueSize = 1000)
  )

}
