package ru.company.api.json

import java.sql.Date
import java.text.SimpleDateFormat

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import ru.company.api.Models.{PetKindEnum, PetModel, PetReport, UserModel}
import ru.company.api.modules.Models.{PetInfo, UserInfo}
import spray.json.{DefaultJsonProtocol, DeserializationException, JsString, JsValue, JsonFormat, RootJsonFormat, deserializationError}

import scala.util.Try

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  implicit object DateFormat extends JsonFormat[Date] {
    def write(date: Date) = JsString(dateToIsoString(date))
    def read(json: JsValue) = json match {
      case JsString(rawDate) =>
        parseIsoDateString(rawDate)
        .fold(deserializationError(s"Expected ISO Date format, got $rawDate"))(identity)
      case error => deserializationError(s"Expected JsString, got $error")
    }
  }

  implicit object StepFormatJsonFormat extends RootJsonFormat[PetKindEnum] {
    def write(obj: PetKindEnum): JsValue = JsString(obj.toString)

    def read(jsonValue: JsValue): PetKindEnum = jsonValue match {
      case JsString(stringObj) => PetKindEnum.withName(stringObj)
      case otherValue => throw new DeserializationException(s"PetKindEnum not found in ${otherValue.toString}")
    }
  }

  private val localIsoDateFormatter = new ThreadLocal[SimpleDateFormat] {
    override def initialValue() = new SimpleDateFormat("yyyy-MM-dd")
  }

  private def dateToIsoString(date: Date) =
    localIsoDateFormatter.get().format(date)

  private def parseIsoDateString(date: String): Option[Date] =
    Try {
      Date.valueOf(date)
    }.toOption


  implicit val FullUserInfoFormat = jsonFormat14(UserInfo)
  implicit val PetInfoUpdateFormat = jsonFormat6(PetInfo)
  implicit val PetModelUpdateFormat = jsonFormat7(PetModel)
  implicit val PetReportUpdateFormat = jsonFormat2(PetReport)
  implicit val UserUpdateFormat = jsonFormat7(UserModel)

}
