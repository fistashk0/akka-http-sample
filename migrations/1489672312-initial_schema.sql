
BEGIN;


CREATE TYPE enum__pet_kind_type AS ENUM(
  'cat',
  'dog'
);

CREATE TABLE tbl_address (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  country text NOT NULL,
  city text NOT NULL,
  street text NOT NULL,
  building text NOT NULL,
  apartment text NOT NULL
);


CREATE TABLE tbl_passport (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  last_name text NOT NULL,
  first_name text NOT NULL,
  patronymic text NOT NULL,
  born DATE NOT NULL,
  number text unique NOT NULL,
  address text NOT NULL,
  department_code text NOT NULL,
  issued_by text NOT NULL
);
create index idx__passport_number on tbl_passport(number);


CREATE TABLE tbl_user (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  last_name text NOT NULL,
  first_name text NOT NULL,
  patronymic text NOT NULL,
  mail_address text unique NOT NULL,
  address BIGINT unique NOT NULL REFERENCES tbl_address (id) ON UPDATE CASCADE ON DELETE RESTRICT,
  passport BIGINT unique NOT NULL REFERENCES tbl_passport (id) ON UPDATE CASCADE ON DELETE RESTRICT
);
create index idx__user_email on tbl_user(mail_address);


CREATE TABLE tbl_pet (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  owner BIGINT NOT NULL REFERENCES tbl_user(id) ON UPDATE CASCADE ON DELETE RESTRICT,
  kind enum__pet_kind_type NOT NULL,
  nickname text NOT NULL,
  born DATE NOT NULL,
  breed text NOT NULL,
  vaccinations text,
 unique (owner, nickname)
);


COMMIT;

-- ==== DOWN ====

BEGIN;

COMMIT;
